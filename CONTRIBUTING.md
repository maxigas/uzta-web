# Contributions guide

## Set up the development enviroment (Debian)

1. Create a dedicated user for the project.
```bash
sudo adduser uzta-web
```

2. Fetch system dependencies.
```bash
sudo apt install git python3-pip python3-dev
```

3. Upgrade pip3 and install virtualenvwrapper from pip.
```bash
sudo pip3 install --upgrade pip
sudo pip3 install virtuaenvwrapper
```

4. Generate a new SSH key and add it to your gitlab user settings.
```bash
sudo su uzta-web
ssh-keygen
```

5. Fork the repo on gitlab and clone your fork.
```bash
git clone git@gitlab.com:[my-user]/uzta-web.git
```

6. Prepare the virtual enviroment
```bash
# set the virtual enviroment directory
echo 'export WORKON_HOME=$HOME/.virtualenvs' >> ~/.bashrc
# set the python version to use
echo 'export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3' >> ~/.bashrc
# source the virtualenv commands
echo 'source /usr/local/bin/virtualenvwrapper.sh' >> ~/.bashrc
# auto start 'uzta' virtual enviroment on 'uzta-web' user login
echo 'workon uzta' >> ~/.bashrc
```

7. Source the new enviroment variables in the working shell
```bash
# This will complain about the 'uzta' enviroment inexistence. Let's create it in the next step!
source ~/.bashrc
```

8. Create 'uzta' virtual enviroment
```bash
mkvirtualenv --no-site-packages -p /usr/bin/python3 uzta
```

9. Install the development dependencies
```bash
pip3 install --upgrade -r requirements/development.txt
```

10. Run the testing server
```bash
./uzta-web.py
```
