<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Uzta, the Lorea scraper</title>
  <link rel="stylesheet" href="/static/uzta.css">
</head>
<body>
<br><br><br>
<div class="page-wrap">
  {{!base}}
</div>
<br><br><br>
<footer class="site-footer">
  <br>
  <h3>This webpage is powered by <a href="https://gitlab.com/calafou/uzta">Uzta</a> software.<br>If you need support you can follow the indications on our repo or just jump into <a href="https://webchat.freenode.net/?channels=uzta">our channel</a> on freenode and say "hello".</h3>
</footer>
</body>
</html>
